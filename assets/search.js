var searchInput = $('input[name=search]');

if(searchInput.length) {

    var client = algoliasearch(searchInput.data('applicationid'), searchInput.data('searchkey'));
    var index = client.initIndex('products');

    $('input[name=search]').autocomplete(
        {
            hint: false,
            autoselect: true,
            debug: true
        }, [
            {
                source: $.fn.autocomplete.sources.hits(index, {hitsPerPage: 10}),
                displayKey: 'name',
                templates: {
                    suggestion: function (suggestion) {
                        return '<a href="?do=algolia-loadProduct&algolia-productId='+ suggestion.objectID +'">'
                            + '<img src="'+suggestion.image+'" style="max-width: 70px">'
                            + suggestion._highlightResult.name.value + '</a>';
                    }
                }
            }
        ]
    ).on('autocomplete:selected', function (event, suggestion, dataset) {
       // console.log(suggestion, dataset);
    });

}
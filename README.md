# Algolia vyhledávání

Napojení eshopu na vyhledávání přes www.algolia.com s využitím https://github.com/algolia/algoliasearch-nette

## Instalace
Composerem si nahrát: algoliasearch-nette

Do composer.json přidat privátní repozitář
```
"repositories": [
    { "type": "vcs", "url": "https://gitlab.com/unio/algolia.git" }
],
```

spustit

```	
composer require unio/algolia
```

Do config.neon pod extensions přidat rozšíření
```neon
extensions:
	algoliaSearch: AlgoliaSearch\Nette\DI\AlgoliaSearchExtension
	algolia: Unio\Algolia\DI\AlgoliaExtension
```
a nastavit si údaje, které nám poskytne algolia -  např. v config.locale.neon
```
algoliaSearch:
    applicationId: XXXXXXXX
    apiKey: XXXXXXXXXXXXXXXXX
    searchOnlyKey: XXXXXXX
```

### Nahrání dat na Algolia.com
Spustit **/admin/algolia/import/categories** a **/admin/algolia/import/products**  (nebo jiné dle namapování presenteru) a nahrát tak data na servery algolia.

### Assety
Pro připojení assetů je řada možností viz https://github.com/algolia/autocomplete.js/#installation

```
yarn add autocomplete.js
```


## Vyhledávání
Je zatím implementováno pouze hledání přes https://github.com/algolia/autocomplete.js/blob/master/README.md

Tam kde chceme hledat (např. BasePresenter.php) si předáme závislost
```php
/**
 * @var AlgoliaSearch\Nette\Client
 * @inject
 */
public $algoliaClient;

/**
 * @var Unio\Algolia\Components\IAlgoliaSearchFactory
 * @inject
 */
public $algoliaSearchFactory;
```

```php

/**
 * @return AlgoliaSearch
 */
protected function createComponentAlgolia()
{
    $searchForm = $this->getComponent('searchForm-search')
        ->setAttribute('data-searchKey', $this->algoliaClient->getSearchOnlyKey())
        ->setAttribute('data-applicationID',  $this->algoliaClient->getContext()->applicationID);
    return $this->algoliaSearchFactory->create();
}
```


<?php
/**
 * Author: Ivo Toman
 */

namespace Unio\Algolia\DI;


use App\DI\CompilerExtension;
use Unio\Navigation\DI\IMenuItemProvider;

class AlgoliaExtension extends CompilerExtension implements IMenuItemProvider
{

	public function getMenuItems()
	{
		return [
			":Algolia:Admin:Import:" => [
				"name" => "algolia.import",
				"icon" => "fa fa-search-plus",
				"parent_identity" => IMenuItemProvider::CONNECTION
			]
		];
	}


	public function loadConfiguration()
	{

		$builder = $this->getContainerBuilder();
		$this->parseConfig($builder, __DIR__ . DIRECTORY_SEPARATOR  . ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.neon");
	}


	public function beforeCompile()
	{

		$builder = $this->getContainerBuilder();

		$this->setPresenterMapping($builder, ['Algolia' => 'Unio\Algolia\*Module\Presenters\*Presenter']);

		$this->addAclResources($builder, ['Algolia:Admin:Import']);
	}


}
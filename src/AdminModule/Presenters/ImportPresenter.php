<?php
/**
 * Author: Ivo Toman
 */

namespace Unio\Algolia\AdminModule\Presenters;

use Nette\Application\Responses\TextResponse;
use Unio\AdminModule\BasePresenter;
use Unio\Algolia\ImportCategories;
use Unio\Algolia\ImportProducts;


/**
 * @cron(categories,products)
 */
class ImportPresenter extends BasePresenter
{
	/**
	 * @var ImportProducts
	 * @inject
	 */
	public $importProducts;

	/**
	 * @var ImportCategories
	 * @inject
	 */
	public $importCategories;


	public function actionCategories()
	{
		$this->importCategories->import();
		$this->sendResponse(new TextResponse('kategorie ok'));
	}


	public function actionProducts()
	{
		$this->importProducts->import();
		$this->sendResponse(new TextResponse('produkty ok'));
	}



}
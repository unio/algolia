<?php
/**
 * Author: Ivo Toman
 */

declare(strict_types=1);

namespace Unio\Algolia;


use AlgoliaSearch\Nette\Client;
use Kdyby\Translation\Translator;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Unio\Eshop\CategoryEntity;
use Unio\Eshop\CategoryManager;


class ImportCategories
{
	use SmartObject;


	/**
	 * @var CategoryManager
	 */
	private $categoryManager;

	/**
	 * @var Client
	 */
	private $algolia;

	/**
	 * @var LinkGenerator
	 */
	private $linkGenerator;

	/**
	 * @var Translator
	 */
	private $translator;


	/**
	 * ImportCategories constructor.
	 * @param Client $algolia
	 * @param CategoryManager $categoryManager
	 * @param LinkGenerator $linkGenerator
	 * @param Translator $translator
	 */
	public function __construct(Client $algolia, CategoryManager $categoryManager, LinkGenerator $linkGenerator, Translator $translator)
	{
		$this->categoryManager = $categoryManager;
		$this->algolia = $algolia;
		$this->linkGenerator = $linkGenerator;
		$this->translator = $translator;
	}


	public function import()
	{
		$index = $this->algolia->initIndex('categories');
		foreach ($this->getSearchableCategories() AS $category) {
			$batch[] = $category;
		}
		$index->saveObjects($batch);
	}


	public function getSearchableCategories(): \Generator
	{
		$result = $this->categoryManager->getActiveTree();
		foreach ($result AS $r) {
			yield $this->mapCategoryToAgolia($r);
		}
	}


	protected function mapCategoryToAgolia(CategoryEntity $r): array
	{
		return [
			'objectID' => (int)$r->id,
			'name' => $r->name_,
			'desc' => (!is_null($r->desc_) ? strip_tags($r->desc_) : null),
			'url' => $this->linkGenerator->link('Eshop:Front:Category:', ['id' => $r->id, 'locale' => $this->translator->getLocale()]),
		];
	}

}
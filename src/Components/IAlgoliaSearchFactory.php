<?php
/**
 * Author: Ivo Toman
 */

namespace Unio\Algolia\Components;


interface IAlgoliaSearchFactory
{
	/** @return AlgoliaSearch */
	function create();
}
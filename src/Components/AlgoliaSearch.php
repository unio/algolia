<?php
/**
 * Author: Ivo Toman
 */

namespace Unio\Algolia\Components;


use Nette\Application\UI\Control;
use Unio\Assets;

class AlgoliaSearch extends Control {

	/**
	 * @var Assets
	 */
	private $assets;

	public function __construct(Assets $assets)
	{
		$this->assets = $assets;
		$this->assets->addCss("/assets/packages/unio/algolia/algolia.css");
	}



	public function handleLoadProduct($productId)
	{
		$this->getPresenter()->redirect(':Eshop:Front:Product:', ['id' => $productId]);
	}



	public function render()
	{
		$this->assets
			->addJs("https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js")
			->addJs("https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js")
			->addJs("/assets/packages/unio/algolia/search.js");

		$this->template->setFile(__DIR__ . '/SearchResults.latte');
		$this->template->render();
	}

}

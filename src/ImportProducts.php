<?php
/**
 * Author: Ivo Toman
 */

declare(strict_types=1);

namespace Unio\Algolia;


use AlgoliaSearch\Nette\Client;
use Kdyby\Translation\Translator;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Unio\Eshop\ProductEntity;
use Unio\Eshop\ProductManager;
use Unio\ImageLocator;


class ImportProducts
{
	use SmartObject;


	/**
	 * @var Client
	 */
	private $algolia;

	/**
	 * @var ProductManager
	 */
	private $productManager;

	/**
	 * @var LinkGenerator
	 */
	private $linkGenerator;

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var ImageLocator
	 */
	private $imageLocator;


	/**
	 * ImportProducts constructor.
	 * @param Client $algolia
	 * @param ProductManager $productManager
	 * @param LinkGenerator $linkGenerator
	 * @param Translator $translator
	 * @param ImageLocator $imageLocator
	 */
	public function __construct(Client $algolia, ProductManager $productManager, LinkGenerator $linkGenerator, Translator $translator, ImageLocator $imageLocator)
	{
		$this->algolia = $algolia;
		$this->productManager = $productManager;
		$this->linkGenerator = $linkGenerator;
		$this->translator = $translator;
		$this->imageLocator = $imageLocator;
	}



	/**
	 * @throws \AlgoliaSearch\AlgoliaException
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	public function import()
	{
		$index = $this->algolia->initIndex('products');

		$i = 0;
		$batch = [];
		foreach ($this->getSearchableProducts() as $product) {
			$batch[] = $product;

			if ($i++ == 500) {
				$index->saveObjects($batch);
				$i = 0;
				$batch = [];
			}

		}
		if (count($batch) > 0) {
			//flush everything else
			$index->saveObjects($batch);
		}
	}


	/**
	 * @return \Generator
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	public function getSearchableProducts(): \Generator
	{
		$result = $this->productManager->findActiveProducts();
		foreach ($result AS $r) {
			yield $this->mapProductToAgolia($r);
		}
	}


	/**
	 * @param ProductEntity $r
	 * @return array
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	protected function mapProductToAgolia(ProductEntity $r): array
	{
		$arr = [
			'objectID' => $r->id,
			'name' => $r->name_,
			'desc' => (!is_null($r->desc_) ? strip_tags($r->desc_) : null),
			'sellcount' => $r->sellcount,
			'showcount' => $r->showcount,
			'catalogue_number' => $r->kat_cislo,
			'status' => $r->status,
			'stock' => $r->sklad,
			'category' => $r->category->name_,
			'url' => $this->linkGenerator->link('Eshop:Front:Product:', ['id' => $r->id, 'locale' => $this->translator->getLocale()]),
			'image' => null,
		];
		$image = $r->getImage();
		if ($image) {
			$arr['image'] = $this->imageLocator->getProductFolder() . $r->getImage()->name;
		}

		return $arr;
	}

}